window.onload = function() {

	/* task_1 */
	function add(a, b) {

		return a + b;
	};

	function subtract(a, b) {

		return a - b;
	};

	function multiplic(a, b) {

		return a * b;
	};

	function modulus(a, b) {

		return a % b;
	};


	/*task_2 */
	var squareEquation = function() {
		var discr = 0;
		var res1 = 0;
		var res2 = 0;
		var a = +prompt('Enter the first number = a') || 2;
		var b = +prompt('Enter the srcond number = b') || 5;
		var c = +prompt('Enter the thurd number = c') || 3;

		discr = b*b - 4*a*c;

		if (discr < 0) {
			console.log('Немає реальних коренів, оскільки дискримінант < 0!');
			return;
		} else {
			discr = Math.sqrt(discr);
			res1 = (-b + discr) / (2 * a);
			res2 = (-b - discr) / (2 * a);

			return [a, b, c, res1, res2];
		}
	};


	/* task_3 */
	var gameStart = function() {
		var start,
			attempt,
			numberIntroduced,
			numberRandom,
			firstAttemptWin,
			secondAttemptWin,
			thirdAttemptWin,
			sumWin,
			continueGame;

		start = confirm('Чи бажаєте почати гру?');

		if (start) {
			console.log('У Вас є 3 спроби щоб вгадати число, good luck!');
			isStartGame();
		} else {
			console.log('Сьогодні ви не виграли мільйон, а могли');
			return;
		}

		function isStartGame() {
			attempt = 3; // Number of attempts to guess the number
			min = 0;
			max = 5;
			firstAttemptWin = 10; // Win from the first attempt
			secondAttemptWin = 5; // Win from the first attempt
			thirdAttemptWin = 2; // Win from the third attempt
			sumWin = 0;
			gameGuessNumber();
		};

		function gameGuessNumber() {

			numberRandom = getRandomInt(min, max);

			numberIntroduced = +prompt('Введіть число');

			if (numberIntroduced === numberRandom) {
				getSumWin(attempt);
				console.log('Ви виграли '+sumWin+'$!');
				isContinueGame();
			} else {
				attempt--;
				checkAttempt(attempt);
			}
		};

		function getRandomInt(min, max) {

			return Math.floor(Math.random() * (max - min)) + min;
		};

		function getSumWin(elem) {

			switch (elem) {
				case 3:
					sumWin += firstAttemptWin;
					break;
				case 2:
					sumWin += secondAttemptWin;
					break;
				case 1:
					sumWin += thirdAttemptWin;
					break;
				default:
					console.log('Error, щось пішло не так...');
			};
		};

		function isContinueGame() {

			continueGame = confirm('Чи бажаєте продовжиты гру? Призи збільшились у три рази...');

			if (continueGame) {
				max *= 2;
				firstAttemptWin *= 3;
				secondAttemptWin *= 3;
				thirdAttemptWin *= 3;
				attempt = 3;
				gameGuessNumber();
			} else {
				console.log('Дякуємо за гру, ваш виграш становить ' +sumWin+ '$!');
			}
		};

		function checkAttempt(elem) {
			var elem = elem;

			if (elem === 2 || elem === 1) {
				console.log('Спроба ' +elem);
				gameGuessNumber();
			} else {
				console.log('Ваш виграш - 0$. Нажаль Ви програли :( ');
				start = confirm('Чи бажаєте почати гру ще раз?');
				isEndGame(start);
			}
		};

		function isEndGame(elem) {

			if (!elem) {
				console.log('До зустрічі!');
			} else {
				isStartGame();
			}
		}
	};


	clickButton();
	/* auxiliary function */
	function clickButton() {
		var page = document.querySelector('.page');

		page.onclick = function(event) {

			elemId = event.target.id;

			if (elemId === 'addition') {
				console.log( add(7, 2) );
			};

			if (elemId === 'subtraction') {
				console.log( subtract(7, 2) );
			};

			if (elemId === 'multiplication') {
				console.log( multiplic(7, 2) );
			};

			if (elemId === 'modulus') {
				console.log( modulus(7, 2) );
			};

			if (elemId === 'squareEquation') {
				var arr = squareEquation();

				console.log( 'Рівняння ' + arr[0] + 'x2 + ' + arr[1] + 'x + ' + arr[2] + 
							' = 0 має 2 розв’язки: x1 = ' + arr[3] +', x2 = ' + arr[4] );
			};

			if (elemId === 'gameStart') {
				gameStart();
			};

		};
	};
};